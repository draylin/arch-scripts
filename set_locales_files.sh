#!/bin/bash

function write_localeconf {
	echo LANG=de_DE.UTF-8 > /etc/locale.conf
	echo LC_COLLATE=C >> /etc/locale.conf
	echo LANGUAGE=de_DE >> /etc/locale.conf
	echo 'Done!'
}

function write_vconsole {
	echo KEYMAP=de-latin1 > /etc/vconsole.conf
	echo FONT=lat9w-16 >> /etc/vconsole.conf
	echo 'Done!'
}



# Get root privileges
if [ ! $UID -eq 0 ]; then
	echo 'You need root-privileges to execute this command. Working with sudo...'
	sudo "$0" "$@" || echo 'Something went wrong. Aborting...'
	exit
fi


echo '#### Setting /etc/locale.conf ####'
if [ -e /etc/locale.conf ]; then
	echo 'The file /etc/locale.conf already exists. Overwrite (y/n)?'
	read dialoginput
	if [ "$dialoginput" != "y" ] && [ "$dialoginput" != "Y" ]; then
		echo 'Skipping /etc/locale.conf...'
	else
		write_localeconf
	fi
else
	write_localeconf
fi

echo '#### Setting /etc/vconsole.conf ####'
if [ -e /etc/locale.conf ]; then
        echo 'The file /etc/vconsole.conf already exists. Overwrite (y/n)?'
        read dialoginput
        if [ "$dialoginput" != "y" ] && [ "$dialoginput" != "Y" ]; then
                echo 'Skipping /etc/vconsole.conf...'
        else
			write_vconsole
	fi
else
	write_vconsole
fi


echo '#### Setting /etc/locale.gen ####'
echo 'Uncommenting the required lines with sed...'
sed -i 's|#de_DE|de_DE|g' /etc/locale.gen
echo 'Done!'

echo '#### Linking time to /etc/localtime ####'
ln -sf '/usr/share/zoneinfo/Europe/Berlin' '/etc/localtime'
echo 'Done!'

echo '#### Generating locales ####'
echo 'Do you want to run locale-gen now? (y/n)'
read dialoginput
if [ "$dialoginput" == "y" ] || [ "$dialoginput" == "Y" ]; then
	locale-gen
fi
