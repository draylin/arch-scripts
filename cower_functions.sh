#!/bin/bash

function getMakedeps() {
	TARGET=$1
	cower -i "${TARGET}" --format %M --listdelim '\n'
}

function getDepends() {
	TARGET=$1
	cower -i "${TARGET}" --format %D --listdelim '\n'
}

function getOptdepends() {
	TARGET=$1
	cower -i "${TARGET}" --format %O --listdelim '\n'
}

function getMissingDepends() {
	TARGET=$1
	comm -23 <(sort <(getDepends "${TARGET}")) <(sort <(pacman -Qq))
}

function getMissingMakedeps() {
	TARGET=$1
	comm -23 <(sort <(getMakedeps "${TARGET}")) <(sort <(pacman -Qq))
}

function getPacmanDepends() {
	TARGET=$1
	comm -12 <(sort <(getDepends "${TARGET}")) <(sort <(pacman -Ssq))
}

function getNonPacmanDepends() {
	TARGET=$1
	comm -23 <(sort <(getDepends "${TARGET}")) <(sort <(pacman -Ssq))
}

function getPacmanMakedeps() {
	TARGET=$1
	comm -12 <(sort <(getMakedeps "${TARGET}")) <(sort <(pacman -Ssq))
}

function getNonPacmanMakedeps() {
	TARGET=$1
	comm -23 <(sort <(getMakedeps "${TARGET}")) <(sort <(pacman -Ssq))
}

function getPacmanOptdepends() {
	TARGET=$1
	comm -12 <(sort <(getOptdepends "${TARGET}")) <(sort <(pacman -Ssq))
}

function getNonPacmanOptdepends() {
	TARGET=$1
	comm -23 <(sort <(getOptdepends "${TARGET}")) <(sort <(pacman -Ssq))
}



