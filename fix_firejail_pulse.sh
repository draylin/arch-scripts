#!/bin/bash

# Set enable-shm in ~/.config/pulse/cliens.conf to "no" in order to fix input and output devices
# disappearing when starting a sound requiring application with firejail.

# Check if any action is necessary
if [ -e "${HOME}/.config/pulse/client.conf" ]; then
	grep -q '^enable-shm = no' "${HOME}/.config/pulse/client.conf" && echo "The value enable-shm is already set to 'no'. Doing nothing..." && exit 1
elif [ ! -d "${HOME}/.config/pulse" ]; then
	echo "Configuration directory of pulse not found, creating '{$HOME}/.config/pulse'..."
	mkdir -p "${HOME}/.config/pulse"
fi

# Add line to ~/.config/pulse/.client.conf
echo 'enable-shm = no' >> "${HOME}/.config/pulse/client.conf"
