#!/bin/bash

# Set environment-variable MUTTER_ENABLE_HYBRID_GPUS to 1 to enable the
# hybrid-gpu support of the display manager of gnome/wayland, 'mutter'

# Check if any action is necessary
grep -q '^MUTTER_ALLOW_HYBRID_GPUS=1$' '/etc/environment' && echo "Hybrid-GPU support already has been allowed. Nothing to do..." && exit 1

# This script must be ran as root. If required use sudo to run this script as root
if [ ! $UID -eq 0 ]; then
	sudo "$0" || echo "This script requires root! Exiting..."
	exit
fi;

# Add line to /etc/environment
echo "MUTTER_ALLOW_HYBRID_GPUS=1" >> /etc/environment

exit 0
