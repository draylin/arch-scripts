#!/bin/bash

# This script disables the pulseaudio session created for gdm
# This is in particular useful if you have problems re-
# connecting bluetooth speakers. It seems like the gdm
# pulseaudio session sometimes takes control over the speakers
# and therefore the user has no access to them.

GDM_PULSE_DIR='/var/lib/gdm/.pulse'
GDM_PULSE_FILE='client.conf'
GDM_USER='gdm'
DISABLE_AUTOSPAWN='autospawn = no'
SET_DAEMON_BINARY='daemon-binary = /bin/true'

changed_something=0

if [ ! ${UID} -eq 0 ]; then
	echo "Script needs to be run as root. Exiting..." && exit 1
fi

if [ ! -d "${GDM_PULSE_DIR}" ]; then
	echo "Directory ${GDM_PULSE_DIR} does not exist. Creating..."
	mkdir -p "${GDM_PULSE_DIR}"
	chown "${GDM_USER}":"${GDM_USER}" "${GDM_PULSE_DIR}"
	changed_something=1
fi

if [ ! -f "${GDM_PULSE_DIR}/${GDM_PULSE_FILE}" ]; then
	echo "File ${GDM_PULSE_DIR}/${GDM_PULSE_FILE} does not exists. Creating..."
	touch "${GDM_PULSE_DIR}/${GDM_PULSE_FILE}"
	chown "${GDM_USER}":"${GDM_USER}" "${GDM_PULSE_DIR}/${GDM_PULSE_FILE}"
	changed_something=1
fi

if grep -q "${DISABLE_AUTOSPAWN}"  "${GDM_PULSE_DIR}/${GDM_PULSE_FILE}"; then
	echo "'${DISABLE_AUTOSPAWN}' is already set in ${GDM_PULSE_DIR}/${GDM_PULSE_FILE}. Doing nothing..."
else
	echo "${DISABLE_AUTOSPAWN}" >> "${GDM_PULSE_DIR}/${GDM_PULSE_FILE}" && echo "Successfuly disabled pulseadio autospawn for gdm!"
	changed_something=1
fi

if grep -q "$SET_DAEMON_BINARY"  "${GDM_PULSE_DIR}/${GDM_PULSE_FILE}"; then
        echo "'${SET_DAEMON_BINARY}' is already set in ${GDM_PULSE_DIR}/${GDM_PULSE_FILE}. Doing nothing..."
else
        echo "${SET_DAEMON_BINARY}" >> "${GDM_PULSE_DIR}/${GDM_PULSE_FILE}" && echo "Successfully changed the daemon binary!"
	changed_something=1
fi

if [ $changed_something -eq 0 ]; then
	echo -
	echo "No changes were applied."
else
	echo -
	echo "Changes applied. Restart your system for changes to take effect."
fi

exit 0
